package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
	private Game game1;
	private Game game2;
	private Game game3;
	private Game game4;
	private Game game5;
	private Game game6;
	
	@Before
	public void initializeGames() throws BowlingException {
		game1 = new Game();
		game1.addFrame(new Frame(10, 0));
		game1.addFrame(new Frame(4, 6));
		game1.addFrame(new Frame(7, 2));
		game1.addFrame(new Frame(3, 6));
		game1.addFrame(new Frame(4, 4));
		game1.addFrame(new Frame(5, 3));
		game1.addFrame(new Frame(3, 3));
		game1.addFrame(new Frame(4, 5));
		game1.addFrame(new Frame(8, 1));
		game1.addFrame(new Frame(2, 6));
		
		// For user story 8
		game2 = new Game();
		game2.addFrame(new Frame(10, 0));
		game2.addFrame(new Frame(10, 0));
		game2.addFrame(new Frame(7, 2));
		game2.addFrame(new Frame(3, 6));
		game2.addFrame(new Frame(4, 4));
		game2.addFrame(new Frame(5, 3));
		game2.addFrame(new Frame(3, 3));
		game2.addFrame(new Frame(4, 5));
		game2.addFrame(new Frame(8, 1));
		game2.addFrame(new Frame(2, 6));
		
		// For user story 9
		game3 = new Game();
		game3.addFrame(new Frame(8, 2));
		game3.addFrame(new Frame(5, 5));
		game3.addFrame(new Frame(7, 2));
		game3.addFrame(new Frame(3, 6));
		game3.addFrame(new Frame(4, 4));
		game3.addFrame(new Frame(5, 3));
		game3.addFrame(new Frame(3, 3));
		game3.addFrame(new Frame(4, 5));
		game3.addFrame(new Frame(8, 1));
		game3.addFrame(new Frame(2, 6));
		
		// For user story 10
		game4 = new Game();
		game4.addFrame(new Frame(1, 5));
		game4.addFrame(new Frame(3, 6));
		game4.addFrame(new Frame(7, 2));
		game4.addFrame(new Frame(3, 6));
		game4.addFrame(new Frame(4, 4));
		game4.addFrame(new Frame(5, 3));
		game4.addFrame(new Frame(3, 3));
		game4.addFrame(new Frame(4, 5));
		game4.addFrame(new Frame(8, 1));
		game4.addFrame(new Frame(2, 8));
		
		// For user story 11
		game5 = new Game();
		game5.addFrame(new Frame(1, 5));
		game5.addFrame(new Frame(3, 6));
		game5.addFrame(new Frame(7, 2));
		game5.addFrame(new Frame(3, 6));
		game5.addFrame(new Frame(4, 4));
		game5.addFrame(new Frame(5, 3));
		game5.addFrame(new Frame(3, 3));
		game5.addFrame(new Frame(4, 5));
		game5.addFrame(new Frame(8, 1));
		game5.addFrame(new Frame(10, 0));
		
		// For user story 12
		game6 = new Game();
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
		game6.addFrame(new Frame(10, 0));
	}
	
	@Test
	// Tests user story 3
	public void testGetGame() throws BowlingException {
		// First frame
		assertEquals(10, game1.getFrameAt(0).firstThrow);
		assertEquals(0, game1.getFrameAt(0).secondThrow);
		// Middle frame
		assertEquals(4, game1.getFrameAt(4).firstThrow);
		assertEquals(4, game1.getFrameAt(4).secondThrow);
		// Last frame
		assertEquals(2, game1.getFrameAt(9).firstThrow);
		assertEquals(6, game1.getFrameAt(9).secondThrow);
	}
	
	@Test(expected = BowlingException.class)
	// Tests getFrameAt when index>9
	public void testGetFrameAtIndexOver9() throws BowlingException {
		game1.getFrameAt(12);
	}
	
	@Test(expected = BowlingException.class)
	// Tests getFrameAt when index<0
	public void testGetFrameAtIndexUnder0() throws BowlingException {
		game1.getFrameAt(-1);
	}
	
	@Test
	// Tests user story 4, 5, 6, 7
	public void testCalculateScore() throws BowlingException {
		assertEquals(103, game1.calculateScore());
	}
	
	@Test
	// Tests user story 8
	public void testCalculateScoreMultipleStrikes() throws BowlingException {
		assertEquals(112, game2.calculateScore());
	}
	
	@Test
	// Tests user story 9
	public void testCalculateScoreMultipleSpares() throws BowlingException {
		assertEquals(98, game3.calculateScore());
	}
	
	@Test
	// Tests user story 10
	public void testCalculateScoreSpareLastFrame() throws BowlingException {
		game4.setFirstBonusThrow(7);
		assertEquals(90, game4.calculateScore());
	}
	
	@Test
	// Tests user story 11
	public void testCalculateScoreStrikeLastFrame() throws BowlingException {
		game5.setFirstBonusThrow(7);
		game5.setSecondBonusThrow(2);
		assertEquals(92, game5.calculateScore());
	}
	
	@Test
	// Tests user story 12
	public void testCalculateScoreBest() throws BowlingException {
		game6.setFirstBonusThrow(10);
		game6.setSecondBonusThrow(10);
		assertEquals(300, game6.calculateScore());
	}
}
