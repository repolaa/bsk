package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	// Tests user story 1
	public void testGetFrame() throws Exception {
		Frame f = new Frame(2, 4);
		assertEquals(2, f.getFirstThrow());
		assertEquals(4, f.getSecondThrow());
	}
	
	@Test
	// Tests user story 2
	public void testGetScore() throws Exception {
		Frame f = new Frame(2, 4);
		assertEquals(6, f.getScore());
	}

}
