package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	List<Frame> frames;
	int firstBonusThrow;
	int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// Empty list
		this.frames = new ArrayList<>();
		
		this.firstBonusThrow=0;
		this.secondBonusThrow=0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size()==10)
			throw new BowlingException("The maximum number of frames (10) has already been reached");
		else
			this.frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index>=0 && index<=9)
			return this.frames.get(index);
		else
			throw new BowlingException("The index must be between 0 and 9");
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(int i = 0; i<10; i++) {
			Frame f = getFrameAt(i);
			if(f.isSpare() && i+1<10)
				f.setBonus(getFrameAt(i+1).getFirstThrow());
			if(f.isStrike() && i+1<10) {
				if(getFrameAt(i+1).isStrike() && i+2<10)
					f.setBonus(getFrameAt(i+1).getScore() + getFrameAt(i+2).getFirstThrow());
				else if(i==8)
					f.setBonus(getFrameAt(i+1).getScore() + firstBonusThrow);
				else
					f.setBonus(getFrameAt(i+1).getScore());
			}
			score = score + f.getScore();
			if(f.isSpare() && i==9)
				score = score + firstBonusThrow;
			if(f.isStrike() && i==9)
				score = score + firstBonusThrow + secondBonusThrow;
		}
		return score;
	}
}
